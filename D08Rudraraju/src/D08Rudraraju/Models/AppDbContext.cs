﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;

namespace D08Rudraraju.Models
{
    public class AppDbContext : DbContext
    {
       public DbSet<Car> Cars { get; set; }
        public DbSet<Location> Locations { get; set; }
    }
}
