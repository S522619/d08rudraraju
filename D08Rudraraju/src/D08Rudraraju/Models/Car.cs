﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace D08Rudraraju.Models
{
    public class Car
    {
        [ScaffoldColumn(false)]
        public int CarId { get; set; }


        [Display(Name = "Name")]
        public string Name { get; set; }


        [Display(Name = "Model")]
        public string Model { get; set; }


        [Display(Name = "Number of Wheels")]
        public int Wheelcount { get; set; }


        [Display(Name = "Color")]
        public String Color { get; set; }


        [Display(Name = "GearType")]
        public String GearType { get; set; }

        [Display(Name = "EngineType")]
        public String EngineType { get; set; }


        [ScaffoldColumn(true)]
        public Int32? LocationID { get; set; }


        public virtual Location Location { get; set; }

        public List<Location> dealerLocation { get; set; }

        public static List<Car> ReadAllFromCSV(string filepath)
        {
            List<Car> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Car.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static Car OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Car item = new Car();

            int i = 0;
            item.Name = Convert.ToString(values[i++]);
            item.Model = Convert.ToString(values[i++]);
            item.Wheelcount = Convert.ToInt32(values[i++]);
            item.Color = Convert.ToString(values[i++]);
            item.GearType = Convert.ToString(values[i++]);
            item.EngineType = Convert.ToString(values[i++]);
            item.LocationID = Convert.ToInt32(values[i++]);

            return item;
        }

    }
}
