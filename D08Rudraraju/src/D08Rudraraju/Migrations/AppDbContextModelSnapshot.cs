using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using D08Rudraraju.Models;

namespace D08Rudraraju.Migrations
{
    [DbContext(typeof(AppDbContext))]
    partial class AppDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("D08Rudraraju.Models.Car", b =>
                {
                    b.Property<int>("CarId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Color");

                    b.Property<string>("EngineType");

                    b.Property<string>("GearType");

                    b.Property<int?>("LocationID");

                    b.Property<string>("Model");

                    b.Property<string>("Name");

                    b.Property<int>("Wheelcount");

                    b.HasKey("CarId");
                });

            modelBuilder.Entity("D08Rudraraju.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<int?>("CarCarId");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("D08Rudraraju.Models.Car", b =>
                {
                    b.HasOne("D08Rudraraju.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });

            modelBuilder.Entity("D08Rudraraju.Models.Location", b =>
                {
                    b.HasOne("D08Rudraraju.Models.Car")
                        .WithMany()
                        .HasForeignKey("CarCarId");
                });
        }
    }
}
